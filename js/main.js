document.addEventListener('DOMContentLoaded', function() {
    objectFit.polyfill({
        selector: 'video',
        fittype: 'cover'
    });
});

$(document).ready(function() {
    $('#submit-form').on('click', function(event) {
        /* Act on the event */
        $('#submit-form').toggleClass('active');
    });

});

